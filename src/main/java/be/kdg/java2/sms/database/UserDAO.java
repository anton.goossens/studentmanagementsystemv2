package be.kdg.java2.sms.database;

import be.kdg.java2.sms.service.User;

public interface UserDAO {
    User getUserByName(String name);

    void addUser(User user);
}
