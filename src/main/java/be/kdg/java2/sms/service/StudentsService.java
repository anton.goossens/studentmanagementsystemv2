package be.kdg.java2.sms.service;

import java.util.List;

public interface StudentsService {
    List<Student> getAllStudents();

    void addStudent(Student student);
}
