package be.kdg.java2.sms.service;

import be.kdg.java2.sms.exceptions.StudentException;

public interface UserService {
    void addUser(String name, String password) throws StudentException;

    boolean login(String username, String password) throws StudentException;
}
